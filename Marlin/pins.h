#ifndef PINS_H
#define PINS_H


#define X_MS1_PIN -1
#define X_MS2_PIN -1
#define Y_MS1_PIN -1
#define Y_MS2_PIN -1
#define Z_MS1_PIN -1
#define Z_MS2_PIN -1
#define E0_MS1_PIN -1
#define E0_MS2_PIN -1
#define E1_MS1_PIN -1
#define E1_MS2_PIN -1
#define DIGIPOTSS_PIN -1

/****************************************************************************************
* Arduino Mega pin assignment
*
****************************************************************************************/
#if MOTHERBOARD == 3 
#define KNOWN_BOARD 1
// 0 31 29 27 25 POLAR good v1
// 1 48 49 50 51 good v1  estruder
// 2 47 40 41 42 good v1  AXE_Z
// 3 45 46 39 38? good v1  AXE_Y

// 4, polar 37 36 35 34 33 32  skip, 31 29 27 25

// 24 - analog pin 8 for TEMP_0_PIN
// 26 - 

#define X_STEP_PIN0         31
#define X_STEP_PIN1         25
#define X_STEP_PIN2         29
#define X_STEP_PIN3         27

#define Y_STEP_PIN0         45
#define Y_STEP_PIN1         46
#define Y_STEP_PIN2         38
#define Y_STEP_PIN3         39

#define Z_STEP_PIN0         47
#define Z_STEP_PIN1         40
#define Z_STEP_PIN2         42
#define Z_STEP_PIN3         41

#define E0_STEP_PIN0         48
#define E0_STEP_PIN1         49
#define E0_STEP_PIN2         50
#define E0_STEP_PIN3         51

//*/

#define X_MIN_PIN           30
#define X_MAX_PIN          -1    //2
#define Y_MIN_PIN          28
#define Y_MAX_PIN          -1    //17
#define Z_MIN_PIN          26
#define Z_MAX_PIN          -1    //19

#define SDPOWER            21
//#define SDSS               53
#define LED_PIN            13
#define PS_ON_PIN          -1
#define KILL_PIN           -1

#ifdef RAMPS_V_1_0 // RAMPS_V_1_0
  #define HEATER_0_PIN     23    // RAMPS 1.0
  #define HEATER_BED_PIN   -1    // RAMPS 1.0
  #define FAN_PIN          11    // RAMPS 1.0
#else // RAMPS_V_1_1 or RAMPS_V_1_2
  #define HEATER_0_PIN     23    // RAMPS 1.1
  #define HEATER_BED_PIN    8    // RAMPS 1.1
  #define FAN_PIN           9    // RAMPS 1.1
#endif
#define HEATER_1_PIN        -1
#define HEATER_2_PIN        -1
#define TEMP_0_PIN          8    // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!!
#define TEMP_1_PIN          -1   
#define TEMP_2_PIN          -1   
#define TEMP_BED_PIN        -1  //1  // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!!




/*


#define X_STEP_PIN0         45
#define X_STEP_PIN1         51
#define X_STEP_PIN2         49
#define X_STEP_PIN3         47

#define Y_STEP_PIN0         24
#define Y_STEP_PIN1         25
#define Y_STEP_PIN2         27
#define Y_STEP_PIN3         26

#define Z_STEP_PIN0         40
#define Z_STEP_PIN1         40
#define Z_STEP_PIN2         40
#define Z_STEP_PIN3         40

#define E0_STEP_PIN0         40
#define E0_STEP_PIN1         40
#define E0_STEP_PIN2         40
#define E0_STEP_PIN3         40

//*/








// my OLD configuration here 

// #define X_STEP_PIN0         45
// #define X_STEP_PIN1         51
// #define X_STEP_PIN2         49
// #define X_STEP_PIN3         47

// #define Y_STEP_PIN0         24
// #define Y_STEP_PIN1         25
// #define Y_STEP_PIN2         27
// #define Y_STEP_PIN3         26

// #define Z_STEP_PIN0         28
// #define Z_STEP_PIN1         29
// #define Z_STEP_PIN2         31
// #define Z_STEP_PIN3         30

// #define E0_STEP_PIN0         34
// #define E0_STEP_PIN1         35
// #define E0_STEP_PIN2         37
// #define E0_STEP_PIN3         36

// //*/

// #define X_MIN_PIN           50
// #define X_MAX_PIN          -1    //2
// #define Y_MIN_PIN          44
// #define Y_MAX_PIN          -1    //17
// #define Z_MIN_PIN          48
// #define Z_MAX_PIN          -1    //19

// #define SDPOWER            21
// //#define SDSS               53
// #define LED_PIN            13
// #define PS_ON_PIN          -1
// #define KILL_PIN           -1

// #ifdef RAMPS_V_1_0 // RAMPS_V_1_0
//   #define HEATER_0_PIN     12    // RAMPS 1.0
//   #define HEATER_BED_PIN   -1    // RAMPS 1.0
//   #define FAN_PIN          11    // RAMPS 1.0
// #else // RAMPS_V_1_1 or RAMPS_V_1_2
//   #define HEATER_0_PIN     46    // RAMPS 1.1
//   #define HEATER_BED_PIN    8    // RAMPS 1.1
//   #define FAN_PIN           9    // RAMPS 1.1
// #endif
// #define HEATER_1_PIN        -1
// #define HEATER_2_PIN        -1
// #define TEMP_0_PIN          8    // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!!
// #define TEMP_1_PIN          -1   
// #define TEMP_2_PIN          -1   
// #define TEMP_BED_PIN        -1  //1  // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!!


// SPI for Max6675 Thermocouple 

#ifndef SDSUPPORT
// these pins are defined in the SD library if building with SD support  
  #define MAX_SCK_PIN          -1
  #define MAX_MISO_PIN         -1
  #define MAX_MOSI_PIN         -1
  #define MAX6675_SS           20
#else
  #define MAX6675_SS           20
#endif//*/

#endif//MOTHERBOARD == 3 || MOTHERBOARD == 33 || MOTHERBOARD == 34


#ifndef KNOWN_BOARD
#error Unknown MOTHERBOARD value in configuration.h
#endif

//List of pins which to ignore when asked to change by gcode, 0 and 1 are RX and TX, do not mess with those!
#define _E0_PINS E0_STEP_PIN0, E0_STEP_PIN0, E0_STEP_PIN0, HEATER_0_PIN, 
/*
#if EXTRUDERS > 1
  #define _E1_PINS E1_STEP_PIN, E1_DIR_PIN, E1_ENABLE_PIN, HEATER_1_PIN,
#else
  #define _E1_PINS
#endif
#if EXTRUDERS > 2
  #define _E2_PINS E2_STEP_PIN, E2_DIR_PIN, E2_ENABLE_PIN, HEATER_2_PIN,
#else
  #define _E2_PINS
#endif
///*/
#ifdef X_STOP_PIN
  #if X_HOME_DIR < 0
    #define X_MIN_PIN X_STOP_PIN
    #define X_MAX_PIN -1
  #else
    #define X_MIN_PIN -1
    #define X_MAX_PIN X_STOP_PIN
  #endif
#endif

#ifdef Y_STOP_PIN
  #if Y_HOME_DIR < 0
    #define Y_MIN_PIN Y_STOP_PIN
    #define Y_MAX_PIN -1
  #else
    #define Y_MIN_PIN -1
    #define Y_MAX_PIN Y_STOP_PIN
  #endif
#endif

#ifdef Z_STOP_PIN
  #if Z_HOME_DIR < 0
    #define Z_MIN_PIN Z_STOP_PIN
    #define Z_MAX_PIN -1
  #else
    #define Z_MIN_PIN -1
    #define Z_MAX_PIN Z_STOP_PIN
  #endif
#endif

#ifdef DISABLE_MAX_ENDSTOPS
#define X_MAX_PIN          -1
#define Y_MAX_PIN          -1
#define Z_MAX_PIN          -1
#endif

#define SENSITIVE_PINS {0, 1, X_STEP_PIN0, X_STEP_PIN0, Y_STEP_PIN0, X_MIN_PIN, X_MAX_PIN, Y_STEP_PIN0, Y_STEP_PIN0, Y_STEP_PIN0, Y_MIN_PIN, Y_MAX_PIN, Z_STEP_PIN0, Z_STEP_PIN0, Z_STEP_PIN0, Z_MIN_PIN, Z_MAX_PIN, PS_ON_PIN, \
                        HEATER_BED_PIN, FAN_PIN,                  \                       
                        _E0_PINS _E0_PINS _E0_PINS             \
                        analogInputToDigitalPin(TEMP_0_PIN), analogInputToDigitalPin(TEMP_1_PIN), analogInputToDigitalPin(TEMP_2_PIN), analogInputToDigitalPin(TEMP_BED_PIN) }
#endif
 //_E0_PINS _E1_PINS _E2_PINS             \



