#include "motors.h"


motors::motors(){  


}

void motors::init(){
    // m = new motors();
   SET_OUTPUT(X_STEP_PIN0);
   SET_OUTPUT(X_STEP_PIN1);
   SET_OUTPUT(X_STEP_PIN2);
   SET_OUTPUT(X_STEP_PIN3);
   MY_set_pins(0,0,0,0,0);

  SET_OUTPUT(Y_STEP_PIN0);
  SET_OUTPUT(Y_STEP_PIN1);
  SET_OUTPUT(Y_STEP_PIN2);
  SET_OUTPUT(Y_STEP_PIN3);
  MY_set_pins(1,0,0,0,0);

  SET_OUTPUT(Z_STEP_PIN0);
  SET_OUTPUT(Z_STEP_PIN1);
  SET_OUTPUT(Z_STEP_PIN2);
  SET_OUTPUT(Z_STEP_PIN3);
  MY_set_pins(2,0,0,0,0);

  SET_OUTPUT(E0_STEP_PIN0);
  SET_OUTPUT(E0_STEP_PIN1);
  SET_OUTPUT(E0_STEP_PIN2);
  SET_OUTPUT(E0_STEP_PIN3);
  MY_set_pins(3,0,0,0,0);
  encsY->write(0);
  // encsY = new Encoder(40,41);

}

void motors::check_dest_steps(){
  for (uint8_t stepper_nomer =0;stepper_nomer<4;stepper_nomer++){  
    int8_t must_go = dest_pos_axes[stepper_nomer] - pos_axes[stepper_nomer];  
    if (must_go != 0){     
      skipped_moove_interrupts[stepper_nomer] = 0;
      bool new_dir = (must_go>0);
      if (!(my_dir_of_axes[stepper_nomer] == new_dir)){
        //slack must be here!
        int8_t tmp = SLACK_AXIS_STEPS_DIR_CHANGE[stepper_nomer] * ((new_dir)? (-1) : (1) );
        // debug_tmp2 = tmp;
        pos_axes[stepper_nomer]+= tmp;
        my_dir_of_axes[stepper_nomer] = new_dir;
      }
      step(stepper_nomer);
    }else{
      skipped_moove_interrupts[stepper_nomer]++;
      if (skipped_moove_interrupts[stepper_nomer] > 100){
        MY_set_pins(stepper_nomer,0,0,0,0); 
      }
    }
  }
}

void motors::fast_interrupt(){
  encsY->read();
  // encs[1]->read();
}

// void motors::SET_DIRECTION(uint8_t stepper_nomer,bool forward){
//   my_dir_of_axes[stepper_nomer]=forward;
// }

void motors::do_step(uint8_t stepper_nomer,bool plan_dir_of_axes){
  if (plan_dir_of_axes){
    dest_pos_axes[stepper_nomer]++;
  }else{
    dest_pos_axes[stepper_nomer]--;
  }
}

void motors::step(uint8_t stepper_nomer){
  debug_tmp1 = pos_axes[stepper_nomer];
  if (my_dir_of_axes[stepper_nomer]){
    pos_axes[stepper_nomer]++;
  }else{
    pos_axes[stepper_nomer]--;
  }
  if ((my_int_for_microstep[stepper_nomer]>3)||(my_int_for_microstep[stepper_nomer]<0))  my_int_for_microstep[stepper_nomer]=0;
             
    // MY_set_pins(stepper_nomer,0,0,0,0);   
    if (my_dir_of_axes[stepper_nomer]){
        my_int_for_microstep[stepper_nomer]++;  
        if (my_int_for_microstep[stepper_nomer]>3){ my_int_for_microstep[stepper_nomer] = 0;} 
    }else{
        if (my_int_for_microstep[stepper_nomer]>0){
             my_int_for_microstep[stepper_nomer]--;
        }else{
             my_int_for_microstep[stepper_nomer] = 3;
        }
    }
    ///*/
     //my_int_for_microstep[stepper_nomer]++;
    
    // if (my_int_for_microstep[stepper_nomer]<0){ my_int_for_microstep[stepper_nomer] = 3;}
      //*/ 
      
    switch (stepper_nomer){ 
      case 0 :
              switch (my_int_for_microstep[stepper_nomer]){
                      case 0 :   MY_set_pins(stepper_nomer,1,0,0,0);   break; 
                      case 1 :   MY_set_pins(stepper_nomer,0,1,0,0);   break; 
                      case 2 :   MY_set_pins(stepper_nomer,0,0,1,0);   break; 
                      case 3 :   MY_set_pins(stepper_nomer,0,0,0,1);   break; 
                      default : break;
               }   
               break;              
       case 1 :     
                 switch (my_int_for_microstep[stepper_nomer]){
                      // case 0 :   MY_set_pins(stepper_nomer,0,1,0,1);   break;  // THIS IS MY // 46 45 38 39
                      // case 1 :   MY_set_pins(stepper_nomer,0,1,1,0);   break; 
                      // case 2 :   MY_set_pins(stepper_nomer,1,0,1,0);   break; 
                      // case 3 :   MY_set_pins(stepper_nomer,1,0,0,1);   break; 

                      case 0 :   MY_set_pins(stepper_nomer,0,1,0,1);   break; 
                      case 1 :   MY_set_pins(stepper_nomer,0,1,1,0);   break; 
                      case 2 :   MY_set_pins(stepper_nomer,1,0,1,0);   break; 
                      case 3 :   MY_set_pins(stepper_nomer,1,0,0,1);   break; 
                      default : break;
                }
                break;
        // case 1 :     
        //          switch (my_int_for_microstep[stepper_nomer]){
        //               case 0 :   MY_set_pins(stepper_nomer,0,1,0,1);   MY_set_pins(2,0,1,0,1);   break; 
        //               case 1 :   MY_set_pins(stepper_nomer,0,1,1,0);   MY_set_pins(2,0,1,1,0);   break; 
        //               case 2 :   MY_set_pins(stepper_nomer,1,0,1,0);   MY_set_pins(2,1,0,1,0);   break; 
        //               case 3 :   MY_set_pins(stepper_nomer,1,0,0,1);   MY_set_pins(2,1,0,0,1);   break; 
        //               default : break;
        //         }
        //         break;
        case 2 :     
                 switch (my_int_for_microstep[stepper_nomer]){
                      case 0 :   MY_set_pins(stepper_nomer,0,1,0,1);   break; 
                      case 1 :   MY_set_pins(stepper_nomer,0,1,1,0);   break; 
                      case 2 :   MY_set_pins(stepper_nomer,1,0,1,0);   break; 
                      case 3 :   MY_set_pins(stepper_nomer,1,0,0,1);   break; 
                      default : break;
                }
                break;
        case 3 :     
                 switch (my_int_for_microstep[stepper_nomer]){
                      case 0 :   MY_set_pins(stepper_nomer,0,1,0,1);   break; 
                      case 1 :   MY_set_pins(stepper_nomer,0,1,1,0);   break; 
                      case 2 :   MY_set_pins(stepper_nomer,1,0,1,0);   break; 
                      case 3 :   MY_set_pins(stepper_nomer,1,0,0,1);   break; 
                      default : break;
                }
                break;
       default : break;    

    }
}

void motors::MY_set_pins(uint8_t stepper_nomer,uint8_t pin_on_off_0,uint8_t pin_on_off_1,uint8_t pin_on_off_2,uint8_t pin_on_off_3)
{
  switch (stepper_nomer)
  {
     case 0 :  
          WRITE(X_STEP_PIN0, pin_on_off_0); 
          WRITE(X_STEP_PIN1, pin_on_off_1); 
          WRITE(X_STEP_PIN2, pin_on_off_2); 
          WRITE(X_STEP_PIN3, pin_on_off_3); 
     break;
     case 1 :  
          WRITE(Y_STEP_PIN0, pin_on_off_0); 
          WRITE(Y_STEP_PIN1, pin_on_off_1); 
          WRITE(Y_STEP_PIN2, pin_on_off_2); 
          WRITE(Y_STEP_PIN3, pin_on_off_3); 
     break;
    case 2 :  
          WRITE(Z_STEP_PIN0, pin_on_off_0); 
          WRITE(Z_STEP_PIN1, pin_on_off_1); 
          WRITE(Z_STEP_PIN2, pin_on_off_2); 
          WRITE(Z_STEP_PIN3, pin_on_off_3); 
     break;
    case 3 :  
          WRITE(E0_STEP_PIN0, pin_on_off_0); 
          WRITE(E0_STEP_PIN1, pin_on_off_1); 
          WRITE(E0_STEP_PIN2, pin_on_off_2); 
          WRITE(E0_STEP_PIN3, pin_on_off_3);   
     break; 
    
     default: break; 
  }
  
    //WRITE(Y_STEP_PIN1, invert_step_pin);
      
} 