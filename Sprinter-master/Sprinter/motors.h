#ifndef motors_h
#define motors_h
#define ENCODER_DO_NOT_USE_INTERRUPTS

#include "motors.h"
#include "fastio.h"
#include "pins.h"
#include "Encoder.h"


class motors{  
public:  
  motors(); 
  uint8_t my_int_for_microstep[4] = {0,0,0,0};
  int16_t dest_pos_axes[4] = {0,0,0,0};
  int8_t  skipped_moove_interrupts[4] = {0,0,0,0};
  int16_t pos_axes[4]      = {0,0,0,0};
  bool    my_dir_of_axes[4] = {0,0,0,0};
  uint16_t debug_tmp1 = 0, debug_tmp2 = 0, debug_tmp3 = 0,calc2=0,proc0_cnt = 0,proc1_cnt = 0,proc2_cnt = 0;
  // int16_t slack_steps[4] = {1, 65,0,0};

  void init();
  void MY_set_pins(uint8_t stepper_nomer,uint8_t pin_on_off_0,uint8_t pin_on_off_1,uint8_t pin_on_off_2,uint8_t pin_on_off_3);
  void do_step(uint8_t stepper_nomer,bool plan_dir_of_axes);


  void fast_interrupt();
  void check_dest_steps();
  Encoder *encsY  = new Encoder(A9,A10);
private: 
  void step(uint8_t stepper_nomer);
  int8_t SLACK_AXIS_STEPS_DIR_CHANGE[4] = {1, 65,0,0};
  // void SET_DIRECTION(uint8_t stepper_nomer,bool forward);
  
};

#endif