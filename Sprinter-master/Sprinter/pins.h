#ifndef PINS_H
#define PINS_H
#define ALARM_PIN          -1

/****************************************************************************************
* Arduino pin assignment
*
*                  ATMega168
*                   +-\/-+
*             PC6  1|    |28  PC5 (AI 5 / D19)
*       (D 0) PD0  2|    |27  PC4 (AI 4 / D18)
*       (D 1) PD1  3|    |26  PC3 (AI 3 / D17)
*       (D 2) PD2  4|    |25  PC2 (AI 2 / D16)
*  PWM+ (D 3) PD3  5|    |24  PC1 (AI 1 / D15)
*       (D 4) PD4  6|    |23  PC0 (AI 0 / D14)
*             VCC  7|    |22  GND
*             GND  8|    |21  AREF
*             PB6  9|    |20  AVCC
*             PB7 10|    |19  PB5 (D 13)
*  PWM+ (D 5) PD5 11|    |18  PB4 (D 12)
*  PWM+ (D 6) PD6 12|    |17  PB3 (D 11) PWM
*       (D 7) PD7 13|    |16  PB2 (D 10) PWM
*       (D 8) PB0 14|    |15  PB1 (D 9)  PWM
*                   +----+


/****************************************************************************************
* Arduino Mega pin assignment
*
****************************************************************************************/
#define KNOWN_BOARD 1

//////////////////FIX THIS//////////////
#ifndef __AVR_ATmega1280__
 #ifndef __AVR_ATmega2560__
 #error Oops!  Make sure you have 'Arduino Mega' selected from the 'Tools -> Boards' menu.
 #endif
#endif



#define X_STEP_PIN0         31
#define X_STEP_PIN1         25
#define X_STEP_PIN2         29
#define X_STEP_PIN3         27

// #define Y_STEP_PIN0         46
// #define Y_STEP_PIN1         45
// #define Y_STEP_PIN2         38
// #define Y_STEP_PIN3         39

#define Y_STEP_PIN0         4
#define Y_STEP_PIN1         5
#define Y_STEP_PIN2         6
#define Y_STEP_PIN3         7

#define Z_STEP_PIN0         47
#define Z_STEP_PIN1         40
#define Z_STEP_PIN2         42
#define Z_STEP_PIN3         41

#define E0_STEP_PIN0         48
#define E0_STEP_PIN1         49
#define E0_STEP_PIN2         50
#define E0_STEP_PIN3         51

#define X_MIN_PIN           30
#define X_MAX_PIN          -1    //2
#define Y_MIN_PIN          28
#define Y_MAX_PIN          -1    //17
#define Z_MIN_PIN          26
#define Z_MAX_PIN          -1    //19


#define SDPOWER            21
#define SDSS               3
#define LED_PIN            13
#define PS_ON_PIN          -1
#define KILL_PIN           -1
#define ALARM_PIN          -1


// #define HEATER_0_PIN     23    // RAMPS 1.1
// #define HEATER_1_PIN      8    // RAMPS 1.1
// #define FAN_PIN           9    // RAMPS 1.1


#define HEATER_1_PIN        -1
#define HEATER_2_PIN        -1
#define TEMP_0_PIN          8    // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!!
#define TEMP_1_PIN          -1   




// #define SDPOWER            48
// #define SDSS               53
// #define LED_PIN            13
// #define PS_ON_PIN          -1
// #define KILL_PIN           -1
// #define ALARM_PIN          -1


// #ifdef RAMPS_V_1_0 // RAMPS_V_1_0
//   #define HEATER_0_PIN     12    // RAMPS 1.0
//   #define HEATER_1_PIN     -1    // RAMPS 1.0
//   #define FAN_PIN          11    // RAMPS 1.0

// #else // RAMPS_V_1_1 or RAMPS_V_1_2
  #define HEATER_0_PIN     23    // RAMPS 1.1
  #define HEATER_1_PIN      8    // RAMPS 1.1
  #define FAN_PIN           9    // RAMPS 1.1
// #endif

// #define TEMP_0_PIN          2    // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!!
// #define TEMP_1_PIN          1    // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!!
// #endif

// SPI for Max6675 Thermocouple 


#define MAX6675_SS       20



#ifndef KNOWN_BOARD
#error Unknown MOTHERBOARD value in configuration.h
#endif


//List of pins which to ignore when asked to change by gcode, 0 and 1 are RX and TX, do not mess with those!
const int sensitive_pins[] = {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, LED_PIN, PS_ON_PIN, HEATER_0_PIN, HEATER_1_PIN, FAN_PIN, TEMP_0_PIN, TEMP_1_PIN};

#endif


